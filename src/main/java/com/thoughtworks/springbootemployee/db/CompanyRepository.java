package com.thoughtworks.springbootemployee.db;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CompanyRepository {

    private List<Company> companyList = new ArrayList<>() {{
        add(new Company(1, "spring"));
        add(new Company(2, "oocl"));
    }};


    public List<Company> getAllCompanies() {
        return companyList;
    }

    public Company getCompanyById(int id) {
        return companyList.stream()
                .filter(company -> company.getId() == id)
                .findFirst()
                .orElse(null);
    }

    public List<Company> getCompaniesByPageSize(int page, int size) {
        return companyList.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public int saveCompany(Company company) {
        int id = nextId();
        company.setId(id);
        companyList.add(company);
        return id;
    }

    private int nextId() {
        int max = companyList.stream()
                .mapToInt(Company::getId)
                .max()
                .orElse(0);
        return max + 1;
    }

    public Company updateCompanyById(int companyId, String name) {
        Company companyById = getCompanyById(companyId);
        companyById.setName(name);
        return companyById;
    }

    public void deleteCompanyById(int companyId) {
        companyList = companyList.stream()
                .filter(company -> company.getId() != companyId).collect(Collectors.toList());
    }
}
