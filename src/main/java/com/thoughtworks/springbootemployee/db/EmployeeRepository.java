package com.thoughtworks.springbootemployee.db;

import com.thoughtworks.springbootemployee.entity.Employee;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class EmployeeRepository {

    private List<Employee> employeeList = new ArrayList<>() {{
        add(new Employee(1, "zhangsan", 50, "female", 8000, 1));
        add(new Employee(2, "lily", 20, "female", 8000, 1));
        add(new Employee(3, "hana", 22, "male", 10000, 2));
        add(new Employee(4, "anna", 25, "male", 6000, 2));
    }};


    public List<Employee> getAllEmployees() {
        return employeeList;
    }

    public Employee getEmployeeById(int id) {
        return employeeList.stream().filter(employee -> employee.getId() == id).findFirst().orElse(null);
    }

    public List<Employee> getEmployeeListByGender(String gender) {
        return employeeList.stream()
                .filter(employee -> employee.getGender().equals(gender))
                .collect(Collectors.toList());
    }

    public int saveEmployee(Employee employee) {
        int id = nextId();
        employee.setId(id);
        employeeList.add(employee);
        return id;
    }

    public Employee updateEmployeeById(Employee employee) {
        Employee dbEmployee = getEmployeeById(employee.getId());
        BeanUtils.copyProperties(employee, dbEmployee);
        return dbEmployee;
    }

    public int deleteEmployeeById(int id) {
        employeeList = employeeList.stream().filter(employee -> employee.getId() != id).collect(Collectors.toList());
        return id;
    }

    public List<Employee> getEmployeeListByPageSize(int page, int size) {
        return employeeList.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public List<Employee> getEmployeesByCompanyId(int companyId) {
        return employeeList.stream()
                .filter(employee -> employee.getCompanyId() == companyId)
                .collect(Collectors.toList());
    }

    private int nextId() {
        Integer max = employeeList.stream()
                .mapToInt(Employee::getId)
                .max()
                .orElse(0);
        return max + 1;
    }
}
