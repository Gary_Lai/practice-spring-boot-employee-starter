package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.db.EmployeeRepository;
import com.thoughtworks.springbootemployee.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("employees")
public class EmployeeController {

    @Autowired
    private EmployeeRepository employeeRepository;

    @GetMapping
    public List<Employee> getEmployeeList() {
        return employeeRepository.getAllEmployees();
    }

    @GetMapping("/{id}")
    public Employee getEmployeeById(@PathVariable int id) {
        return employeeRepository.getEmployeeById(id);
    }

    @GetMapping(params = "gender")
    public List<Employee> getEmployeeByGender(@RequestParam String gender) {
        return employeeRepository.getEmployeeListByGender(gender);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public int addEmployee(@RequestBody Employee employee) {
        return employeeRepository.saveEmployee(employee);
    }

    @PutMapping("/{id}")
    public Employee updateEmployee(@PathVariable int id, @RequestParam int age, @RequestParam int salary) {
        return employeeRepository.updateEmployeeById(new Employee(id, null, age, null, salary));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public int deleteEmployee(@PathVariable int id) {
        return employeeRepository.deleteEmployeeById(id);
    }

    @GetMapping(params = {"page", "size"})
    public List<Employee> getEmployeesByPage(@RequestParam int page, @RequestParam int size) {
        return employeeRepository.getEmployeeListByPageSize(page, size);
    }
}
