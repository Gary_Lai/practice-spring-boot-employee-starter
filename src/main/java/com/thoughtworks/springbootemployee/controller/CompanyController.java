package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.db.CompanyRepository;
import com.thoughtworks.springbootemployee.db.EmployeeRepository;
import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/companies")
public class CompanyController {

    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private EmployeeRepository employeeRepository;

    @GetMapping
    public List<Company> getAllCompanies() {
        return companyRepository.getAllCompanies();
    }

    @GetMapping("/{id}")
    public Company getCompanyById(@PathVariable int id) {
        return companyRepository.getCompanyById(id);
    }

    @GetMapping("/{id}/employees")
    public List<Employee> getAllEmployeesInCompany(@PathVariable(name = "id") int companyId) {
        return employeeRepository.getEmployeesByCompanyId(companyId);
    }

    @GetMapping(params = {"page", "size"})
    public List<Company> getCompaniesByPageSize(@RequestParam int page, @RequestParam int size) {
        return companyRepository.getCompaniesByPageSize(page, size);
    }

    @PostMapping
    public int addCompany(@RequestBody Company company) {
        return companyRepository.saveCompany(company);
    }

    @PutMapping("/{companyId}")
    public Company updateCompanyById(@PathVariable int companyId, @RequestParam String name) {
        return companyRepository.updateCompanyById(companyId, name);
    }

    @DeleteMapping("/{companyId}")
    public void deleteCompanyById(@PathVariable int companyId) {
        companyRepository.deleteCompanyById(companyId);
    }
}
