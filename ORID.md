##  Daily Summary 

**Time: 2023/7/18**

**Author: 赖世龙**

---

**O (Objective):**   This morning, I learned http and restful, and this afternoon, I learned springboot and pair development, and did a lot of pair exercises with my classmates

**R (Reflective):**  Satisfied

**I (Interpretive):**  Through the pair development with classmates, I can know the programming ideas of classmates, learn a lot of ideas of others, and at the same time, I can show my ideas to others

**D (Decisional):**  Pair development is a very cool thing. I can do pair development with my classmates in the future, and learn their programming ideas through communication

